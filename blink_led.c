/*
 * blink_led.c
 *
 *  Created on: Oct 30, 2020
 *      Author: djg-m
 */


#include "blink_led.h"
#include "main.h"

td_blink_led pcb_led;
td_blink_led g_led;

void led_init(td_blink_led * led, GPIO_TypeDef * gpio_port, uint16_t gpio_pin,
		uint32_t time_hi, uint32_t time_low, uint8_t cycles){
	led->gpio_port = gpio_port;
	led->gpio_pin = gpio_pin;
	led->time_hi = time_hi;
	led->time_low = time_low;
	led->cycles = cycles;
	led->current_round = cycles;
}

void blinking_led(td_blink_led * led){
	switch(led->state){
		case IDLE:
			break;
		case HI:
			led_hi(led);
			break;
		case LOW:
			led_low(led);
			break;
		default:
			break;
	}
}


void led_hi(td_blink_led *led) {
	HAL_GPIO_WritePin(led->gpio_port, led->gpio_pin, GPIO_PIN_SET);
	if (led->current_round < led->cycles) {
		if ((HAL_GetTick() - led->last_tick) > led->time_hi) {
			led->state = LOW;
			led->last_tick = HAL_GetTick();
		}
		else {
			return;
		}
	} else {
		HAL_GPIO_WritePin(led->gpio_port, led->gpio_pin, GPIO_PIN_RESET);
		led->state = IDLE;
		led->last_tick = HAL_GetTick();
	}
	led->current_round++;
}

void led_low(td_blink_led * led){
	HAL_GPIO_WritePin(led->gpio_port, led->gpio_pin, GPIO_PIN_RESET);
	if ((HAL_GetTick() - led->last_tick) > led->time_low){
		led->state = HI;
		led->last_tick = HAL_GetTick();
	}
}

void led_start(td_blink_led * led){
	led->state = HI;
	led->last_tick = HAL_GetTick();
	led->current_round = 0;
}
