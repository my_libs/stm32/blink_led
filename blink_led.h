/*
 * blink_led.h
 *
 *  Created on: Oct 30, 2020
 *      Author: djg-m
 */

#ifndef BLINK_LED_H_
#define BLINK_LED_H_

#include "main.h"

typedef enum{
	IDLE = 0,
	LOW = 1,
	HI = 2,
}td_led_state;

typedef struct blink_led{
	td_led_state state;
	uint8_t cycles;
	uint8_t current_round;
	uint32_t time_hi;
	uint32_t time_low;
	uint32_t last_tick;
	uint32_t timer;
	GPIO_InitTypeDef * gpio_port;
	uint16_t gpio_pin;
} td_blink_led;

void led_init(td_blink_led * led, GPIO_TypeDef * gpio_port, uint16_t gpio_pin,
		uint32_t time_hi, uint32_t time_low, uint8_t cycles);

void blinking_led(td_blink_led * led);
void led_hi(td_blink_led * led);
void led_low(td_blink_led * led);
void led_start(td_blink_led * led);


#endif /* BLINK_LED_H_ */
