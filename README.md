# blink_led

Biblioteka do błyskania ledem, nieblokująca. Umożliwia ustawienie czasu włączenia, wyłączenia oraz liczbę cykli (mignięć).


Do inicjacji leda potrzebne są:

- czas trwania stanu zaświecenia (HI)
- czas trwania zgaszenia (LOW)
- liczba cykli (włącz/wyłącz)
- adres portu GPIO
- numer pinu GPIO

`led_init(adres led, adres_GPIO, nr_pin, czas_hi, czas_low, liczba_cykli` 

W głównej pętli `while (1)` umieścić trzeba polecenie `blinking_led(adres_led)`.

Wyzwolenie błyskania następuje po poleceniu `led_start(adres_led)`.

